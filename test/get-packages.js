import { expect } from 'chai';
import getPackages from '../src/get-packages';

describe('getMessages', () => {
  it('gathers 3 unique packages', () => {
    expect(getPackages([
      { message: 'fix(foo): fix component\n\naffects: a' },
      { message: 'fix(foo): fix component\n\naffects: b' },
      { message: 'fix(foo): fix component\n\naffects: c' },
    ])).to.deep.equal(['a', 'b', 'c']);
  });
  it('gathers 3 unique packages with duplicates', () => {
    expect(getPackages([
      { message: 'fix(foo): fix component\n\naffects: x' },
      { message: 'fix(foo): fix component\n\naffects: y' },
      { message: 'fix(foo): fix component\n\naffects: y' },
      { message: 'fix(foo): fix component\n\naffects: z' },
    ])).to.deep.equal(['x', 'y', 'z']);
  });
  it('gathers 3 unique packages with multiple per line', () => {
    expect(getPackages([
      { message: 'fix(foo): fix component\n\naffects: a' },
      { message: 'fix(foo): fix component\n\naffects: foo, bar' },
    ])).to.deep.equal(['a', 'foo', 'bar']);
  });
  it('gathers 2 unique packages with multiple per line', () => {
    expect(getPackages([
      { message: 'fix(foo): fix component\n\naffects: a' },
      { message: 'fix(foo): fix component\n\naffects: b, a' },
    ])).to.deep.equal(['a', 'b']);
  });
  it('gathers no packages when none specified', () => {
    expect(getPackages([
      { message: 'fix(foo): fix component' },
      { message: 'fix(foo): fix component' },
    ])).to.deep.equal([]);
  });
  it('gathers unique packages with scopes', () => {
    expect(getPackages([
      { message: 'fix(foo): fix component\n\naffects: @foo/bar' },
      { message: 'fix(foo): fix component\n\naffects: @foo/bar, @foo/baz, buzz' },
    ])).to.deep.equal(['@foo/bar', '@foo/baz', 'buzz']);
  });
});
