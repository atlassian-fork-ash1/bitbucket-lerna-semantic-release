import { findAffectsLine, isRelevant } from 'lerna-semantic-release-analyze-commits';
import conventionalCommitsParser from 'conventional-commits-parser';
import conventionalChangelogWriter from 'conventional-changelog-writer';
import through from 'through2';

/**
 * Copied from conventional-changelog-angular
 * Ideally we'd use conventional-changelog-angular directly, but we can't because
 * it isn't written for browsers.
 */
import footerPartial from 'conventional-changelog-angular/templates/footer.hbs';
import commitPartial from 'conventional-changelog-angular/templates/commit.hbs';
import templatePartial from 'conventional-changelog-angular/templates/template.hbs';

import parserOpts from './conventional-changelog-opts/parser-opts';
import writerOpts from './conventional-changelog-opts/writer-opts';

export default function getChangelog(commits, pkg) {
  return new Promise((resolve) => {
    const relevantCommitMessages = commits
      .filter(commit => isRelevant(findAffectsLine(commit), pkg))
      .map(({ message }) => message);

    const stream = through();
    relevantCommitMessages.forEach((message) => {
      stream.write(message);
    });
    stream.end();
    stream.pipe(conventionalCommitsParser(parserOpts))
      .pipe(conventionalChangelogWriter({}, Object.assign(writerOpts, {
        headerPartial: '',
        footerPartial,
        commitPartial,
        templatePartial,
      })))
      .pipe(through((chunk, enc, cb) => {
        resolve(chunk.toString());
        cb(null);
      }));
  });
}
