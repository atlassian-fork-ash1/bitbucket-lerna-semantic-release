import { analyze } from 'lerna-semantic-release-analyze-commits';

export default function getReleaseType(commits, pkg) {
  return new Promise((resolve, reject) => {
    analyze({
      pkg: { name: pkg },
      commits,
    }, (err, type) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(type);
    });
  });
}
