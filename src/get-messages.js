export default function (commits) {
  return commits.map(commit => commit.message);
}
